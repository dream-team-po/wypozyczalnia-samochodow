<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_model extends CI_Model {

  public function __construct(){
    parent::__construct();
    $this->load->database();
  }


  public function new_question($data){
    return $this->db->insert('kontakt', $data);
  }



  public function get_user_id_from_username($username){
    $this->db->select('id');
    $this->db->from('uzytkownik');
    $this->db->where('username', $username);

    return $this->db->get()->row('id');
  }
}
