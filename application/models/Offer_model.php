<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Offer_model extends CI_Model {

  public function __construct(){
    parent::__construct();
    $this->load->database();
  }

  public function create_offer($data){
    return $this->db->insert('oferta', $data);
  }

  public function update_offer($data){
    $this->db->where('id', $data['id']);
    unset($data['id']);

    return $this->db->update('oferta', $data);
  }

  public function delete_offer($id){
    $this->db->where('id', $id);

    return $this->db->delete('oferta');
  }

  // funkcja pobierająca wszystko o ofercie za pomocą ID
  public function get_offer($id){
    $this->db->from('oferta');
    $this->db->where('id', $id);

    return $this->db->get()->row();
  }

  public function get_offer_list($per_page, $offset){
    $this->db->select('*');
    $this->db->from('oferta');
    $this->db->join('samochod', 'oferta.samochod_id = samochod.id');
    if($per_page != null){
        $this->db->limit($per_page, $offset);
    }
    return $this->db->get()->result();
  }

  public function count_offer_list(){
      $this->db->select('COUNT(*)');
      $this->db->from('oferta');
      $this->db->join('samochod', 'oferta.samochod_id = samochod.id');
      return $this->db->count_all_results();
  }

  public function get_offer_info($id){
    $this->db->select('*');
    $this->db->from('oferta');
    $this->db->where('oferta.id', $id);
    $this->db->join('samochod', 'oferta.samochod_id = samochod.id');
    return $this->db->get()->row();
  }

  public function search($data){
    $this->db->select('*');
    $this->db->from('oferta');
    $this->db->join('samochod', 'oferta.samochod_id = samochod.id');
    $this->db->group_start();

    if(!empty($data['marka'])){
      $this->db->like('samochod.marka', $data['marka']);
    }

    if(!empty($data['model'])){
      $this->db->like('samochod.model', $data['model']);
    }

    if(!empty($data['typ'])){
      $this->db->like('samochod.typ', $data['typ']);
    }

    if(!empty($data['cena-od']) && !empty($data['cena-do'])){
      $this->db->where("oferta.cena BETWEEN ".$data['cena-od']." AND ".$data['cena-do']);
    }else{
      if(!empty($data['cena-od'])){
        $this->db->where('oferta.cena >=', $data['cena-od']);
      }

      if(!empty($data['cena-do'])){
        $this->db->where('oferta.cena <=', $data['cena-do']);
      }
    }

    $this->db->group_end();
    return $this->db->get()->result();
  }

  public function count_search($data){
      $this->db->select('*');
      $this->db->from('oferta');
      $this->db->join('samochod', 'oferta.samochod_id = samochod.id');
      $this->db->group_start();

      if(!empty($data['marka'])){
          $this->db->like('samochod.marka', $data['marka']);
      }

      if(!empty($data['model'])){
          $this->db->like('samochod.model', $data['model']);
      }

      if(!empty($data['typ'])){
          $this->db->like('samochod.typ', $data['typ']);
      }

      if(!empty($data['cena-od']) && !empty($data['cena-do'])){
          $this->db->where("oferta.cena BETWEEN ".$data['cena-od']." AND ".$data['cena-do']);
      }else{
          if(!empty($data['cena-od'])){
              $this->db->where('oferta.cena >=', $data['cena-od']);
          }

          if(!empty($data['cena-do'])){
              $this->db->where('oferta.cena <=', $data['cena-do']);
          }
      }

      $this->db->group_end();
      return $this->db->count_all_results();
  }

  public function get_offer_id_from_car_id($car_id){
      $this->db->select('id');
      $this->db->from('oferta');
      $this->db->where('samochod_id', $car_id);

      return $this->db->get()->row('id');
  }

  public function get_car_models_from_car_mark($mark){
    $this->db->distinct();
    $this->db->select('model');
    $this->db->from('samochod');
    $this->db->where('marka', $mark);

    return $this->db->get()->result();
  }

  public function get_car_marks(){
    $this->db->distinct();
    $this->db->select('marka');
    $this->db->from('samochod');

    return $this->db->get()->result();
  }

  public function get_car_types(){
    $this->db->distinct();
    $this->db->select('typ');
    $this->db->from('samochod');

    return $this->db->get()->result();
  }

  public function check_if_can_add_opinion($username, $offer_id){
    $user_id = $this->get_user_id_from_username($username);
    $car_id = $this->get_car_id_from_offer_id($offer_id);
    $this->db->select('*');
    $this->db->from('wypozyczenie');
    $this->db->where('uzytkownik_id', $user_id);
    $this->db->where('samochod_id', $car_id);
    $result = $this->db->get()->result();

    return ($result == null) ? false : true;
  }

  private function get_user_id_from_username($username){
    $this->db->select('id');
    $this->db->from('uzytkownik');
    $this->db->where('username', $username);

    return $this->db->get()->row('id');
  }

  private function get_car_id_from_offer_id($offer_id){
    $this->db->select('samochod_id');
    $this->db->from('oferta');
    $this->db->where('id', $offer_id);

    return $this->db->get()->row('samochod_id');
  }

}
