<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Car_model extends CI_Model {

  public function __construct(){
    parent::__construct();
    $this->load->database();
  }

  public function create_car($data){
    return $this->db->insert('samochod', $data);
  }

  public function update_car($data){
    $this->db->where('id', $data['id']);
    unset($data['id']);

    return $this->db->update('samochod', $data);
  }

  public function delete_car($id){
    $this->db->where('id', $id);

    return $this->db->delete('samochod');
  }

  // funkcja pobierająca wszystko o samochodzie za pomocą ID
  public function get_car($id){
    $this->db->from('samochod');
    $this->db->where('id', $id);

    return $this->db->get()->row();
  }

  public function get_car_list(){
    return $this->db->get('samochod')->result();
  }

  public function get_car_id_from_offer_id($offer_id){
    $this->db->select('samochod_id');
    $this->db->from('oferta');
    $this->db->where('id', $offer_id);

    return $this->db->get()->row('samochod_id');
  }

  public function get_user_id_from_username($username){
    $this->db->select('id');
    $this->db->from('uzytkownik');
    $this->db->where('username', $username);

    return $this->db->get()->row('id');
  }

  public function get_price_from_offer_id($offer_id){
    $this->db->select('cena');
    $this->db->from('oferta');
    $this->db->where('id', $offer_id);

    return $this->db->get()->row('cena');
  }

  public function check_date_range($offer_id ,$start, $end){
    $car_id = $this->get_car_id_from_offer_id($offer_id);
    $this->db->select('*');
    $this->db->from('wypozyczenie');
    $this->db->where('samochod_id = "'.$car_id.'" AND DATE(data_wypozyczenia) <= "'.$start.'" AND DATE(data_zwrotu) > "'.$start.'"');
    $this->db->or_where('samochod_id = "'.$car_id.'" AND DATE(data_wypozyczenia) <= "'.$end.'" AND DATE(data_zwrotu) > "'.$end.'"');
    $this->db->or_where('samochod_id = "'.$car_id.'" AND DATE(data_wypozyczenia) > "'.$start.'" AND DATE(data_zwrotu) <= "'.$end.'"');
    $result = $this->db->get()->result();

    return ($result == null) ? true : false;
  }

  public function create_rent($data){
    $this->db->select("datediff('".$data['data_zwrotu']."', '".$data['data_wypozyczenia']."') * t2.cena as total_price");
    $this->db->from('wypozyczenie as t1');
    $this->db->from('oferta as t2');
    $this->db->from('samochod as t3');
    $this->db->where('t3.id = t2.samochod_id');
    $this->db->where('t1.samochod_id = t3.id');
    $data['do_zaplaty'] = $this->db->get()->row('total_price');
    //INSERT INTO `wypozyczenie`(`samochod_id`, `uzytkownik_id`, `data_wypozyczenia`, `data_zwrotu`, `do_zaplaty`) VALUES (1, 2, '2019-03-27', '2019-03-30', (select datediff(t1.data_zwrotu, t1.data_wypozyczenia) * t2.cena as total_price from wypozyczenie as t1, oferta as t2, samochod as t3 where t3.id = t2.samochod_id AND t1.samochod_id = t3.id))
    return $this->db->insert('wypozyczenie', $data);
  }

  public function get_reserved_date_ranges($offer_id){
    $car_id = $this->get_car_id_from_offer_id($offer_id);
    $this->db->select('date_format(data_wypozyczenia, "%Y-%m-%d") as start, date_format(data_zwrotu, "%Y-%m-%d") as end');
    $this->db->from('wypozyczenie');
    $this->db->where('samochod_id', $car_id);

    return $this->db->get()->result();
  }

}
