<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

  public function __construct(){
    parent::__construct();
    $this->load->database();
  }



  public function create_user($data){
    // hashowanie hasła
    $data['haslo'] = password_hash($data['haslo'], PASSWORD_BCRYPT);

    return $this->db->insert('uzytkownik', $data);
  }



  // funkcja sprawdzająca dane logowania
  public function check_user_login($username, $password){
		$this->db->select('haslo');
		$this->db->from('uzytkownik');
		$this->db->where('username', $username);
		$hash = $this->db->get()->row('haslo');

    return password_verify($password, $hash);
  }



  public function get_user_id_from_username($username){
    $this->db->select('id');
    $this->db->from('uzytkownik');
    $this->db->where('username', $username);

    return $this->db->get()->row('id');
  }



  // funkcja pobierająca wszystko o użytkowniku za pomocą ID
  public function get_user($id){
    $this->db->from('uzytkownik');
    $this->db->where('id', $id);

    return $this->db->get()->row();
  }



  public function get_user_profile($username){
    $this->db->select('username, email, imie, nazwisko, ulica, nr_mieszkania, miejscowosc');
    $this->db->from('uzytkownik');
    $this->db->where('username', $username);

    return $this->db->get()->row();
  }



  public function update_user($data){
    $this->db->where('username', $data['username']);
    return $this->db->update('uzytkownik', $data);
  }



  public function get_navbar_permission($is_admin){
    // 1 - użytkownik
    // 2 - admin
    if($is_admin == true){
      // *użytkownik jest adminem*
      return 2;
    }else{
      // *użytkownik nie jest adminem*
      return 1;
    }
  }



}
