<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Opinion_model extends CI_Model {

  public function __construct(){
    parent::__construct();
    $this->load->database();
  }



  public function create_opinion($data, $username){
    $data['uzytkownik_id'] = $this->get_user_id_from_username($username);
    $data['samochod_id'] = $this->get_car_id_from_offer_id($data['samochod_id']);
    $this->db->set($data);
    return $this->db->insert('opinia', $data);
  }



  public function check_if_was_renting($username, $offer_id){
    $user_id = $this->get_user_id_from_username($username);
    $car_id = $this->get_car_id_from_offer_id($offer_id);
    $this->db->select('*');
    $this->db->from('wypozyczenie');
    $this->db->where('uzytkownik_id', $user_id);
    $this->db->where('samochod_id', $car_id);
    $result = $this->db->get()->result();

    return ($result == null) ? false : true;
  }



  public function get_opinion_list($offer_id, $per_page, $offset){
    $car_id = $this->get_car_id_from_offer_id($offer_id);
    $this->db->select('opinia.*, uzytkownik.username');
    $this->db->from('opinia');
    $this->db->where('samochod_id', $car_id);
    $this->db->join('uzytkownik', 'opinia.uzytkownik_id = uzytkownik.id');
    if($per_page != null) $this->db->limit($per_page, $offset);

    return $this->db->get()->result();
  }



  public function get_offer_info($id){
    $this->db->select('*');
    $this->db->from('oferta');
    $this->db->where('oferta.id', $id);
    $this->db->join('samochod', 'oferta.samochod_id = samochod.id');
    return $this->db->get()->row();
  }


  public function avg_rating($offer_id){
    $car_id = $this->get_car_id_from_offer_id($offer_id);
    $this->db->select('ROUND(AVG(ocena), 2) as avg_rating');
    $this->db->from('opinia');
    $this->db->where('samochod_id', $car_id);
    $result = $this->db->get()->row('avg_rating');

    return ($result != null) ? $result : 0;
  }



  public function count_opinion_list($offer_id){
    $car_id = $this->get_car_id_from_offer_id($offer_id);
    $this->db->select('opinia.*, uzytkownik.username');
    $this->db->from('opinia');
    $this->db->where('samochod_id', $car_id);
    $this->db->join('uzytkownik', 'opinia.uzytkownik_id = uzytkownik.id');

    return $this->db->count_all_results();
  }



  private function get_user_id_from_username($username){
    $this->db->select('id');
    $this->db->from('uzytkownik');
    $this->db->where('username', $username);

    return $this->db->get()->row('id');
  }



  private function get_car_id_from_offer_id($offer_id){
    $this->db->select('samochod_id');
    $this->db->from('oferta');
    $this->db->where('id', $offer_id);

    return $this->db->get()->row('samochod_id');
  }

}
