<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

    <!-- informacje zwrotne - błędy przy walidacji danych z formularza -->
    <?php if (validation_errors()) : ?>
      <div class="alert alert-danger" role="alert">
        <?= validation_errors() ?>
      </div>
    <?php endif ?>

    <!-- informacje zwrotne - błędy ogólne -->
    <?php if (isset($error)) : ?>
      <div class="alert alert-danger" role="alert">
        <?= $error ?>
      </div>
    <?php endif ?>

    <!-- informacje zwrotne - sukces -->
    <?php if (isset($success)) : ?>
      <div class="alert alert-success" role="alert">
        <?= $success ?>
      </div>
    <?php endif ?>

  <div class="container py-5">
    <div class="dark-form-content col-width-50">
      <div class="dark-form-header">
        <h2>Logowanie</h2>
      </div>
      <div class="dark-form-body col-width-100">
        <form method="POST">
          <label>Nazwa użytkownika</label>
          <input type="username" class="form-control" placeholder="Nazwa użytkownika" name="username">
          <label>Hasło</label>
          <input type="password" class="form-control" placeholder="Hasło" name="password">
          <button class="btn btn-lg light-blue-button btn-block" type="submit">Zaloguj się</button>
        </form>
      </div>
    </div>
  </div>
