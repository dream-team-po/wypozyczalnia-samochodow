<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="text-center">
  <div class="container">
    <div class="offer-info-container">
      <div class="offer-info-image-window">
        <div class="offer-info-image-window-background1"></div>
        <div class="offer-info-image-window-background2"></div>
      </div>
      <div class="offer-info-image">
        <img src="<?= $image ?>"/>
      </div>
      <div class="offer-info-content">
        <div class="offer-info-header-row">
          <h1><?= $result->marka." ".$result->model ?></h1>
          <h1 class="offer-info-price"><?= $result->cena."zł" ?></h1>
        </div>
        <p>
          <i class="material-icons">settings</i>
          <span>Silnik: <?= $result->silnik ?></span>
        </p>
        <p>
          <i class="material-icons">today</i>
          <span>Rok produkcji: <?= $result->rok_produkcji ?></span>
        </p>
        <p>
          <i class="material-icons">directions_car</i>
          <span>Typ nadwozia: <?= $result->typ ?></span>
        </p>
        <p>
          <i class="material-icons">list_alt</i>
          <span>Wyposażenie: <?= $result->wyposazenie ?></span>
        </p>
        <div class="offer-info-description">
          <h1>Opis</h1>
          <?= $result->opis ?></span>
        </div>
      </div>
      <div class="offer-info-buttons-row">
        <button onclick="window.location.href='/opinion/list/<?= $offer_id ?>'" class="btn btn-lg offer-info-button">Zobacz opinie</button>
        <?php if($opinion): ?>
          <button onclick="window.location.href='/opinion/add/<?= $offer_id ?>'" class="btn btn-lg offer-info-button">Dodaj opinie</button>
        <?php endif ?>
        <?php if($this->session->navbar == 2): ?>
          <!-- opcje administratora -->
          <button onclick="window.location.href='/offer/edit/<?= $offer_id ?>'" class="btn btn-lg offer-info-button">Edytuj ofertę</button>
          <button onclick="offer_delete(<?= $offer_id ?>)" class="btn btn-lg offer-info-button" >Usuń ofertę</button>
          <!-- opcje administratora -->
        <?php endif ?>
        <button onclick="window.location.href='/car/rent/<?= $offer_id ?>'" class="btn btn-lg offer-info-button ml-auto">wypożycz</button>
      </div>
    </div>
  </div>
</div>
<script>
  function offer_delete(offer_id){
    var r = confirm("Czy na pewno chcesz usunąć tę ofertę?");
    if(r){
      if(offer_id != null){
        var url = '/offer/delete/' + offer_id;
        document.location.href = url;
      }
    }
  }
</script>
