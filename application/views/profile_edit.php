<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
  <div class="container edycja-konta-padding">
    <div class="row kafelki-oferty edycja-konta-padding col-width-40">
      <div class="text-center col-width-80">
        <img class="col-width-100" src="/images/logo_small.png" alt="" width="72" height="72">
        <h2>Zmiana danych konta</h2>
      </div>
      <form class="needs-validation mx-auto col-width-80 py-3" method:"POST">
        <!-- username -->
        <div class="py-1">
          <label for="username">Login</label>
          <input type="username" class="form-control" id="username" placeholder="login" name="username" value="<?= $result->username ?>">
        </div>
        <!-- email -->
        <div class="py-1">
          <label for="email">E-mail</label>
          <input type="email" class="form-control" id="email" placeholder="nazwa@gmail.com" name="email" value="<?= $result->email ?>">
        </div>
        <!-- dane opcjonalne -->
        <!-- imie -->
        <div class="py-1">
          <label for="imie">Imie</label>
          <input type="imie" class="form-control" id="imie" placeholder="Twoje imię" name="imie" value="<?= $result->imie ?>">
        </div>
        <!-- nazwisko -->
        <div class="py-1">
          <label for="nazwisko">Nazwisko</label>
          <input type="nazwisko" class="form-control" id="nazwisko" placeholder="Twoje nazwisko" name="nazwisko" value="<?= $result->nazwisko ?>">
        </div>
        <!-- ulica -->
        <div class="py-1">
          <label for="ulica">Ulica</label>
          <input type="ulica" class="form-control" id="ulica" placeholder="np. Krakowska 51" name="ulica" value="<?= $result->ulica ?>">
        </div>
        <!-- nr mieszkania -->
        <div class="py-1">
          <label for="nr_mieszkania">Numer mieszkania</span></label>
          <input type="nr_mieszkania" class="form-control" id="nr_mieszkania" placeholder="np. 3" name="nr_mieszkania" value="<?= $result->nr_mieszkania ?>">
        </div>
        <!-- miejscowosc -->
        <div class="py-1">
          <label for="miejscowosc">Miejscowość</label>
          <input type="miejscowosc" class="form-control" id="miejscowosc" placeholder="np. Opole" name="miejscowosc" value="<?= $result->miejscowosc ?>">
        </div>
        <div class="py-3">
          <button class="btn btn-lg btn-primary btn-block" type="submit">Zapisz</button>
        </div>
      </form>
    </div>
  </div>
