<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
  <!-- informacje zwrotne - błędy przy walidacji danych z formularza -->
  <?php if (validation_errors()) : ?>
    <div class="alert alert-danger" role="alert">
      <?= validation_errors() ?>
    </div>
  <?php endif ?>

  <!-- informacje zwrotne - błędy ogólne -->
  <?php if (isset($error)) : ?>
    <div class="alert alert-danger" role="alert">
      <?= $error ?>
    </div>
  <?php endif ?>

  <!-- informacje zwrotne - sukces -->
  <?php if (isset($success)) : ?>
    <div class="alert alert-success" role="alert">
      <?= $success ?>
    </div>
  <?php endif ?>

  <div class="container py-5 text-center">
    <div class="dark-form-content  col-width-50">
      <div class="dark-form-header">
        <h2>Dodaj opinię</h2>
      </div>
      <div class="dark-form-body"> <!-- wyśrodkowanie -->
        <form class="col-width-90 py-2 " method="POST">
          <select class="custom-select mr-sm-2 form-control" name="ocena">
            <option selected>Wybierz ocenę</option>
            <option value="1">1 / 5</option>
            <option value="2">2 / 5</option>
            <option value="3">3 / 5</option>
            <option value="4">4 / 5</option>
            <option value="5">5 / 5</option>
          </select>
          <textarea class="form-control" placeholder="Opinia (maks. 2000 znaków)" rows="5" id="opis" name="opis"></textarea>
          <div class="py-3">
            <button class="btn btn-lg light-blue-button btn-block" type="submit">Dodaj opinię</button>
          </div>
        </form>
      </div>
    </div>
  </div>
