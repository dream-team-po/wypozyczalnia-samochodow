<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

  <div class="d-flex align-items-center justify-content-center flex-wrap col-color-light ">
    <form method="POST" class="szer-proc">
      <div class="form-row">
        <div class="col py-4">
          <input type="text" class="form-control" placeholder="Marka">
        </div>
        <div class="col py-4">
          <input type="text" class="form-control" placeholder="Model">
        </div>
        <div class="col py-4">
          <input type="text" class="form-control" placeholder="Typ">
        </div>
      </div>
      <button type="button" class="btn btn-dark mx-auto float-right">Szukaj</button>
    </form>
  </div>
