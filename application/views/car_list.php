<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Editable table -->
<div class="card">
  <h3 class="card-header text-center font-weight-bold text-uppercase py-4">Lista Samochodów</h3>
  <div class="card-body">
    <div id="table" class="table-editable">

      <table class="table table-bordered table-responsive-md table-striped text-center">
        <tbody>
          <tr>
            <th class="text-center">Marka</th>
            <th class="text-center">Model</th>
            <th class="text-center">Nadwozie</th>
            <th class="text-center">Numer rejestracyjny</th>
            <th class="text-center">Rok Produkcji</th>
            <th class="text-center">Silnik</th>
	          <th class="text-center">Numer VIN</th>
            <th class="text-center">Wyposażenie</th>
            <th class="text-center">Dostępność</th>
            <th class="text-center">Edytuj</th>
	          <th class="text-center">Usuń</th>
          </tr>
          <?php foreach($result as $object) : ?>
          <?php $row = json_decode(json_encode($object), TRUE); ?>
          <tr>
            <td class="pt-3-half" contenteditable="false"><?= $row['marka'] ?></td>
            <td class="pt-3-half" contenteditable="false"><?= $row['model'] ?></td>
            <td class="pt-3-half" contenteditable="false"><?= $row['typ'] ?></td>
            <td class="pt-3-half" contenteditable="false"><?= $row['num_rejestracyjny'] ?></td>
            <td class="pt-3-half" contenteditable="false"><?= $row['rok_produkcji'] ?></td>
            <td class="pt-3-half" contenteditable="false"><?= $row['silnik'] ?></td>
  	        <td class="pt-3-half" contenteditable="false"><?= $row['vin'] ?></td>
  	        <td class="pt-3-half" contenteditable="false"><?= $row['wyposazenie'] ?></td>
  	        <td class="pt-3-half" contenteditable="false"><?= $row['dostepnosc'] ?></td>
            <td>
              <a href="/car/edit/<?= $row['id'] ?>" type="button" class="btn light-blue-button btn-sm my-0">Edytuj</a>
            </td>
  		      <td>
              <button type="button" onclick="car_delete(<?= $row['id'] ?>)" class="btn light-blue-button btn-sm my-0">Usuń</button>
            </td>
          </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<!-- Editable table -->
<script>
function car_delete(car_id){
  var r = confirm("Czy na pewno chcesz usunąć ten samochód?");
  if(r){
    if(car_id != null){
      var url = '/car/delete/' + car_id;
      document.location.href = url;
    }
  }
}
</script>
