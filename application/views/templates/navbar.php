<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <!--Navbar -->
    <nav class="navbar navbar-expand-lg navbar-dark elegant-color-dark grey darken-4">
      <a class="navbar-brand-override" href="/">
        <img src="/images/logo.png" width="180" height="40" alt="">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4"
        aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent-4">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link-blue" href="/">Strona główna</a>
          </li>
          <li class="nav-item">
            <a class="nav-link-blue" href="/offer/list">Oferta</a>
          </li>
          <!--
          <li class="nav-item">
            <a class="nav-link" href="#">Pricing</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
              aria-expanded="false">Dropdown
            </a>
            <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="#">Action</a>
              <a class="dropdown-item" href="#">Another action</a>
              <a class="dropdown-item" href="#">Something else here</a>
            </div>
          </li>
          -->
        </ul>
        <ul class="navbar-nav ml-auto nav-flex-icons">
          <li class="nav-item">
            <a class="nav-link-blue" href="/user/register">Zarejestruj</a>
          </li>
          <li class="nav-item">
            <a class="nav-link-blue" href="/user/login">Zaloguj</a>
          </li>
        </ul>
      </div>
    </nav>
    <!--Navbar -->
