<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <!--Navbar -->
    <nav class="navbar navbar-expand-lg navbar-dark elegant-color-dark grey darken-4">
      <a class="navbar-brand-override" href="/">
        <img src="/images/logo.png" width="180" height="40" alt="">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4"
        aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent-4">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link-blue" href="/">Strona główna</a>
          </li>
          <li class="nav-item">
            <a class="nav-link-blue" href="/offer/list">Oferta</a>
          </li>
          <li class="nav-item">
            <a class="nav-link-blue" href="/contact/question">Kontakt</a>
          </li>

        </ul>
        <ul class="navbar-nav ml-auto nav-flex-icons">
          <li class="nav-item dropdown">
            <a class="nav-link-blue dropdown-toggle" id="navbarDropdownMenuLink-4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fas fa-user"></i> <span class=""><?= $this->session->username ?></span> </a>
              <div class="dropdown-menu dropdown-menu-right dropdown-info user-dropdown-body">
                <div class="user-dropdown-header">
                  <div class="user-dropdown-avatar">
                    <img class="rounded-circle shadow-sm" src="/images/avatar.jpg" height="100" width="100"/>
                  </div>
                  <div class="user-dropdown-header-content">
                    <div>
                      <h4><?= $this->session->imie ?> <?= $this->session->nazwisko ?></h4>
                      <p>
                        <i class="material-icons">mail</i>
                        <span><?= $this->session->email ?></span>
                      </p>
                      <p>
                        <i class="material-icons">place</i>
                        <span><?= $this->session->miejscowosc ?></span>
                      </p>
                      <p>
                        <i class="material-icons">place</i>
                        <span><?= "ul. " . $this->session->ulica . "/" . $this->session->nr_mieszkania ?></span>
                      </p>
                    </div>
                  </div>
                </div>
                <a href="/user/edit" class="user-dropdown-button">
                  <i class="material-icons">edit</i>
                  <span>Edytuj</span>
                </a>
                <a href="/user/logout" class="user-dropdown-button">
                  <i class="material-icons">exit_to_app</i>
                  <span>Wyloguj</span>
                </a>
              </div>
            </li>
          </ul>
        </div>
      </nav>
      <!--Navbar -->
