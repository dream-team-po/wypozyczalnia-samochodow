<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
  <div class="container">
    <div class="opinion-list-container">
      <div class="opinion-list-image-window">
        <div class="opinion-list-image-window-background1"></div>
        <div class="opinion-list-image-window-background2"></div>
      </div>
      <div class="opinion-list-image">
        <img src="<?= $image ?>"/>
      </div>
      <div class="opinion-list-content">
        <div class="opinion-list-header-row">
          <h1><?php echo $offer_info->marka." ".$offer_info->model ?></h1>
          <h1 class="opinion-list-rating">
            <i class="material-icons">star</i>
            <?php echo $avg_rating." / 5.00" ?>
          </h1>
        </div>
        <?php foreach($result as $row) : ?>
          <div class="opinion-info-container">
            <img class="opinion-info-avatar" src="/images/avatar.jpg"/>
            <div class="opinion-info-content">
              <h4><?php echo $row->username ?></h4>
              <p><?php echo $row->opis ?></p>
            </div>
            <div class="opinion-info-rating">
              <p><?php echo "Ocena: ".$row->ocena."/5" ?></p>
            </div>
          </div>
        <?php endforeach; ?>
      </div>
    </div>
  </div>
  <?php if($pagination) : ?>
    <div class="pagination-bar">
      <?php echo $pagination ?>
    </div>
  <?php endif ?>
