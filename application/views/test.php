<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>


  <div class="container rejestracja-padding">
    <div class="dark-form-content  col-width-50">
      <div class="dark-form-header">
        <h2>Edycja profilu</h2>
      </div>
      <div class="row kafelki-oferty dark-form-body font-description"> <!-- wyśrodkowanie -->
        <form class="needs-validation mx-auto col-width-100" method="post" novalidate>
          <!-- username -->
          <label class="font-description">Username</label>
          <input type="text" class="form-control" id="username" name="username" value="<?= $this->session->username ?>" readonly>
          <!-- email -->
          <label class="font-description">Adres Email</label>
          <input type="email" class="form-control" id="email" placeholder="Email" value="<?= $this->session->email ?>" name="email">
          <!-- dane opcjonalne -->
          <!-- imie -->
          <label class="font-description">Imię</label>
          <input type="imie" class="form-control" id="imie" placeholder="Twoje imię" value="<?= $result->imie ?>" name="imie">
          <!-- nazwisko -->
          <label class="font-description">Nazwisko</label>
          <input type="nazwisko" class="form-control" id="nazwisko" placeholder="Twoje nazwisko" value="<?= $result->nazwisko ?>" name="nazwisko">
          <!-- ulica -->
          <label class="font-description">Ulica oraz numer budynku</label>
          <input type="ulica" class="form-control" id="ulica" placeholder="Ulica oraz numer budynku (np. Krakowska 51)" value="<?= $result->ulica ?>" name="ulica">
          <!-- nr mieszkania -->
          <label class="font-description">Numer mieszkania</label>
          <input type="nr_mieszkania" class="form-control" id="nr_mieszkania" placeholder="Numer mieszkania (np. 3)" value="<?= $result->nr_mieszkania ?>" name="nr_mieszkania">
          <!-- miejscowosc -->
          <label class="font-description">Miejscowość</label>
          <input type="miejscowosc" class="form-control" id="miejscowosc" placeholder="Miejscowość (np. Opole)" value="<?= $result->miejscowosc ?>" name="miejscowosc">
          <div class="py-2">
            <button class="btn btn-lg light-blue-button btn-block" type="submit">Zapisz</button>
          </div>
        </form>
      </div>
    </div>
  </div>
