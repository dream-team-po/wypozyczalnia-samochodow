<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <!-- informacje zwrotne - błędy przy walidacji danych z formularza -->
    <?php if (validation_errors()) : ?>
      <div class="alert alert-danger" role="alert">
        <?= validation_errors() ?>
      </div>
    <?php endif ?>

    <!-- informacje zwrotne - błędy ogólne -->
    <?php if (isset($error)) : ?>
      <div class="alert alert-danger" role="alert">
        <?= $error ?>
      </div>
    <?php endif ?>

    <!-- informacje zwrotne - sukces -->
    <?php if (isset($success)) : ?>
      <div class="alert alert-success" role="alert">
        <?= $success ?>
      </div>
    <?php endif ?>
    <div class="container py-5">
      <div class="row dark-form-content col-width-50"> <!-- wyśrodkowanie -->
        <div class="dark-form-header">
          <h2>Edycja oferty</h2>
        </div>
        <div class="col-width-100 dark-form-body">
          <form method="POST" id="form">
            <label>ID samochodu</label>
            <input id="samochod_id" class="form-control" placeholder="ID samochodu" name="samochod_id" value="<?= $result->samochod_id ?>">
            <label>Opis</label>
  		      <textarea class="form-control" placeholder="Opis oferty (maks. 2000 znaków)" rows="10" id="opis" name="opis" form="form"><?= $result->opis ?></textarea>
            <label>Cena</label>
            <input id="cena" class="form-control" placeholder="Cena za jeden dzień" name="cena" value="<?= $result->cena ?>">
            <div class="py-2 text-center">
              <button class="btn light-blue-button" type="submit">Zapisz</button>
            </div>
          </form>
        </div>
      </div>
    </div>
