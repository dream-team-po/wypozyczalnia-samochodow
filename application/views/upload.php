<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>


  <div class="container ">
    <form method="POST" enctype="multipart/form-data" action="" accept-charset="utf-8">
      <input type="text" id="text" name="text">
      <input type="file" id="userfile" name="userfile">
      <button class="btn light-blue-button" type="submit">Wyślij</button>
    </form>
  </div>
