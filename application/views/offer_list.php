<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
  <!-- Wyszukiwanie -->
  <div class="text-center">
    <div class="d-flex align-items-center justify-content-center flex-wrap wyszukiwarka">
      <form method="POST" class="szer-proc">
        <div class="form-row">
          <div class="col">
            <div class="py-2">
              <select class="form-control" id="marka" name="marka">
              <option disabled selected hidden>Marka</option>

              <?php foreach($marks as $object) : ?>
                <?php $row = json_decode(json_encode($object), TRUE); ?>
                <option value="<?= $row['marka'] ?>"><?= $row['marka'] ?></option>
              <?php endforeach; ?>

              </select>
            </div>
            <div class="py-2">
              <input type="text" class="form-control" placeholder="Cena od" name="cena-od">
            </div>
          </div>
          <div class="col">
            <div class="py-2">
              <select class="form-control" id="model" name="model" disabled>
                <option disabled selected hidden>Model</option>
              </select>
            </div>
            <div class="py-2">
              <input type="text" class="form-control" placeholder="Cena do" name="cena-do">
            </div>
          </div>
          <div class="col">
            <div class="py-2">
              <select class="form-control" id="typ" name="typ">
              <option disabled selected hidden>Typ nadwozia</option>
                <?php foreach($types as $object) : ?>
                  <?php $row = json_decode(json_encode($object), TRUE); ?>
                    <option value="<?= $row['typ'] ?>"><?= $row['typ'] ?></option>
                <?php endforeach; ?>
              </select>
            </div>
            <div class="py-2">
              <button type="submit" class="btn btn-sm light-blue-button mx-auto float-right" name="submit">Szukaj</button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
  <!-- Wyszukiwanie -->


  <div class="container">
    <?php foreach($result as $row) : ?>
      <div class="offer-item-container">
        <div class="offer-item-image-window">
          <div class="offer-item-image-window-background1"></div>
          <div class="offer-item-image-window-background2"></div>
        </div>
        <div class="offer-item-image">
          <img src="<?= $row->image ?>"/>
        </div>
        <div class="offer-item-content">
          <div class="offer-item-header-row">
            <h2><?= $row->marka." ".$row->model ?></h2>
            <h2 class="offer-item-price"><?= $row->cena."zł" ?></h2>
          </div>
          <p>
            <i class="material-icons">directions_car</i>
            <span>Silnik: <?= $row->silnik ?></span>
          </p>
          <p>
            <i class="material-icons">today</i>
            <span>Rok produkcji: <?= $row->rok_produkcji ?></span>
          </p>
            <button class="btn btn-sm offer-item-button" onclick="window.location.href='/offer/info/<?= $row->ID ?>'">
              zobacz ofertę
            </button>
        </div>
      </div>
    <?php endforeach; ?>
  </div>
  <?php if($pagination) : ?>
    <div class="pagination-bar">
    <?= $pagination ?>
    </div>
  <?php endif ?>

<script>
  $(document).ready(function() {

    // przechwycenie eventu
    $('#marka').change(getCarModels);

    function getCarModels(){
      var mark = $('#marka').val();
      $.ajax({
        url: '/offer/search',
        method: 'post',
        data: { marka: mark },
        success: function(result){
          // konwersja stringa na JSON
          var result = JSON.parse(result);

          // usuwanie wszystkich opcji z modeli
          $('#model').children('option:not(:first)').remove();

          // dodanie opcji "Model" jako placeholder
          $('#model')
            .append($("<option></option>")
            .attr("disabled", true)
            .attr("selected", true)
            .attr("hidden", true)
            .text("Model"));

          // włączenie selekcji modeli
          $('#model').attr('disabled', false);

          // dodawanie opcji wyboru modeli
          $.each(result.models, function(key, value) {
            $('#model')
              .append($("<option></option>")
              .attr("value",value.model)
              .text(value.model));
          });

        },
        error: function(x,e) {
          if (x.status==0) {
            alert('You are offline!!\n Please Check Your Network.');
          } else if(x.status==404) {
            alert('Requested URL not found.');
          } else if(x.status==500) {
            alert('Internel Server Error.');
          } else if(e=='parsererror') {
            alert('Error.\nParsing JSON Request failed.');
          } else if(e=='timeout'){
            alert('Request Time out.');
          } else {
            alert('Unknow Error.\n'+x.responseText);
          }
        }
      });
    }

  });
</script>
