<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

  <div class="text-center">
    <!-- informacje zwrotne - błędy przy walidacji danych z formularza -->
    <?php if (validation_errors()) : ?>
      <div class="alert alert-danger" role="alert">
        <?= validation_errors() ?>
      </div>
    <?php endif ?>

    <!-- informacje zwrotne - błędy ogólne -->
    <?php if (isset($error)) : ?>
      <div class="alert alert-danger" role="alert">
        <?= $error ?>
      </div>
    <?php endif ?>

    <!-- informacje zwrotne - sukces -->
    <?php if (isset($success)) : ?>
      <div class="alert alert-success" role="alert">
        <?= $success ?>
      </div>
    <?php endif ?>
  </div>
  <div class="container py-5 ">
    <div class="dark-form-content col-width-50">
      <div class="dark-form-header">
        <h2>Wypożycz samochód</h2>
      </div>
      <div class="dark-form-body">
        <form method="POST">
          <label>Termin wypożyczeń</label>
          <input type="text" class="form-control" placeholder="Termin wypożyczeń" name="termin" id="termin" autocomplete="off" readonly>
          <div class="d-flex justify-content-between">
            <div class="pr-1">
              <label>Cena za dzień</label>
              <input type="text" class="form-control" autocomplete="off" value="<?= $price ?>" id="price" readonly>
            </div>
            <div class="pl-1">
              <label>Ilość dni</label>
              <input type="text" class="form-control" autocomplete="off" value="" id="days_amount" readonly>
            </div>
          </div>
          <label>Cena brutto</label>
          <input type="text" class="form-control" autocomplete="off" value="" id="final_price" readonly>
          <button class="btn btn-lg light-blue-button btn-block" type="submit">wypożycz</button>
          <input type="number" autocomplete="off" value="<?= $offer_id ?>" id="offer_id" readonly hidden disabled>
        </form>
      </div>
    </div>
  </div>

  <script type="text/javascript">
    $(document).ready(function() {

      // ustawienie języka polskiego dla datepickera
      moment.locale('pl');

      // pobranie zakresu zarezerwowanych dat
      var date_ranges = getDateRanges();

      // uruchomienie i konfiguracja datepickera
      $('#termin').daterangepicker({
        locale: {
          format: 'YYYY-MM-DD',
        },
        minDate: moment(),
        startDate: moment(),
        endDate: moment().add(1, 'days'),
        isInvalidDate: function(date) {
          for(var i = 0; i < date_ranges.length; i++){
            var disabled_start = moment(date_ranges[i]['start'], 'YYYY-MM-DD');
            var disabled_end = moment(date_ranges[i]['end'], 'YYYY-MM-DD');
            if(date.isSame(disabled_start) || date.isAfter(disabled_start) && date.isBefore(disabled_end)){
              return true;
            }
          }
        }
      });

      // jednorazowe wykonanie funkcji
      getFinalPrice();

      // przechwycenie eventu
      $('#termin').change(getFinalPrice);

      // funkcja wyliczająca ilość dni z zakresu dat
      function getDaysAmount(str){
        var start = str.substring(0,10);
        var end = str.substring(13,23);
        var dateStart = moment(start, "YYYY-MM-DD");
        var dateEnd = moment(end, "YYYY-MM-DD");
        var days = moment.duration(dateEnd.diff(dateStart)).asDays();
        return days;
      }

      // funkcja wyliczająca ostateczną cenę
      function getFinalPrice(){
        var price = $('#price').val();
        var dateRange = $('#termin').val();
        var days = getDaysAmount(dateRange);
        $('#days_amount').val(days);
        $('#final_price').val(days * price);
      }

      // funkcja pobierająca zakres zarezerwowanych dat
      function getDateRanges(){
        var offer_id = $('#offer_id').val();
        $.ajax({
          url: '/car/reserved_date_ranges',
          method: 'post',
          data: {offer_id: offer_id},
          success: function(result){
            // konwersja stringa na JSON
            var result = JSON.parse(result);
            getResult(result);
        },
        error: function(x,e) {
          if (x.status==0) {
              alert('You are offline!!\n Please Check Your Network.');
          } else if(x.status==404) {
              alert('Requested URL not found.');
          } else if(x.status==500) {
              alert('Internel Server Error.');
          } else if(e=='parsererror') {
              alert('Error.\nParsing JSON Request failed.');
          } else if(e=='timeout'){
              alert('Request Time out.');
          } else {
              alert('Unknow Error.\n'+x.responseText);
          }
          }
        });
      }

      // funkcja przypisująca dane do zmiennej
      // konieczne ze względu na asychroniczność AJAXa
      function getResult(data){
        date_ranges = data['reserved_date_ranges'];
      }

    });
  </script>
