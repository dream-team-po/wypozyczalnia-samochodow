<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>


      <div class="container py-5">
        <div class="text-center">
          <h1><b>TEMAT</b></h1>
        </div>
        <div class="row">

          <div class="col-width-80">
            <div class="col-md-10 offset-md-1 row-block background-color-white opinie-odstepy contact-pytanie float-left">
              <div class="media">
                <div class="media-left align-self-center py-1">
                  <img class="rounded-circle" src="/images/avatar.jpg" height="80px" width="80px">
                </div>
                <div class="media-body align-self-center text-center">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>
                </div>
                <div class="media-right align-self-center">

                </div>
              </div>
            </div>
          </div>
        <div class="col-width-80">
          <div class="col-md-10 offset-md-1 row-block background-color-white opinie-odstepy contact-odpowiedz float-right">
            <div class="media">
              <div class="media-left align-self-center py-1">

              </div>
              <div class="media-body align-self-center text-center">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>
              </div>
              <div class="media-right align-self-center">
                <img class="rounded-circle" src="/images/avatar.jpg" height="80px" width="80px">
              </div>
            </div>
          </div>
        </div>
          <div class="col-width-80 py-5">
            <div class="col-width-50">
              <form method="POST">
                <textarea class="form-control" placeholder="Treść odpowiedzi (maks. 5000 znaków)" rows="7" id="pytanie" name="pytanie"></textarea>
                <button class="btn btn-lg light-blue-button btn-block" type="submit">Odpowiedz</button>
              </form>
            </div>
          </div>
        </div>
      </div>
