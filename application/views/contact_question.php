<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
  <!-- informacje zwrotne - błędy przy walidacji danych z formularza -->
  <?php if (validation_errors()) : ?>
    <div class="alert alert-danger" role="alert">
      <?= validation_errors() ?>
    </div>
  <?php endif ?>

  <!-- informacje zwrotne - błędy ogólne -->
  <?php if (isset($error)) : ?>
    <div class="alert alert-danger" role="alert">
      <?= $error ?>
    </div>
  <?php endif ?>

  <!-- informacje zwrotne - sukces -->
  <?php if (isset($success)) : ?>
    <div class="alert alert-success" role="alert">
      <?= $success ?>
    </div>
  <?php endif ?>

  <div class="container py-5">
    <div class="dark-form-content col-width-60">
      <div class="dark-form-header">
        <h2>Zadaj pytanie</h2>
      </div>
      <div class="dark-form-body col-width-100">
        <form method="POST" id="form">
          <label>Temat</label>
          <input type="username" class="form-control" placeholder="Temat" name="temat">
          <label>Treść pytania</label>
          <textarea class="form-control" placeholder="Treść pytania (maks. 5000 znaków)" rows="7" id="pytanie" name="pytanie" form="form"></textarea>
          <button class="btn btn-lg light-blue-button btn-block" type="submit">Wyślij</button>
        </form>
      </div>
    </div>
  </div>
