<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>


  <div class="container">
    <div class="row">
      <div class="col-md-10 offset-md-1 row-block background-color-white opinie-odstepy">
        <div class="media">
          <div class="media-left align-self-center py-1">
            <img class="rounded-circle" src="/images/avatar.jpg" height="80px" width="80px">
          </div>
          <div class="media-body align-self-center text-center">
            <h2><b>Temat pytania</b></h2>
          </div>
          <div class="media-right align-self-center">
            <a href="#" class="btn btn-default">Otwórz</a>
          </div>
        </div>
      </div>
    </div>
  </div>
