<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
  <div class="container edycja-konta-padding">
    <div class="row dark-form-content col-width-50">
      <div class="dark-form-header text-center">
        <div class="d-flex align-item-center justify-content-center">
          <img class="rounded-circle" src="/images/avatar.jpg" height="100px" width="100px">
        </div>
        <label class="profile-header h2"><?= $result->username ?></label>
      </div>
      <form class="needs-validation mx-auto col-width-80 dark-form-body profile-dane" method="POST">
        <div class="">
          <input type="username" class="form-control" id="username" placeholder="login" name="username" value="<?= $result->username ?>" readonly>
        </div>
        <div class="">
          <label for="email" class="h4">Adres e-mail</label>
          <input type="email" class="form-control" id="email" placeholder="nazwa@gmail.com" name="email" value="<?= $result->email ?>" readonly>
        </div>
        <div class="py-1">
          <button class="btn btn-lg btn-primary btn-block" type="submit">Edytuj swoje dane</button>
        </div>
        <div class="py-1">
          <button class="btn btn-lg btn-primary btn-block" type="submit">Historia wypożyczeń</button>
        </div>
        <div class="py-1">
          <button class="btn btn-lg btn-primary btn-block" type="submit">Usuń swoje konto</button>
        </div>
    </div>
  </div>
