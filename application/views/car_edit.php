<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

    <!-- informacje zwrotne - błędy przy walidacji danych z formularza -->
    <?php if (validation_errors()) : ?>
      <div class="alert alert-danger" role="alert">
        <?= validation_errors() ?>
      </div>
    <?php endif ?>

    <!-- informacje zwrotne - błędy ogólne -->
    <?php if (isset($error)) : ?>
      <div class="alert alert-danger" role="alert">
        <?= $error ?>
      </div>
    <?php endif ?>

    <!-- informacje zwrotne - sukces -->
    <?php if (isset($success)) : ?>
      <div class="alert alert-success" role="alert">
        <?= $success ?>
      </div>
    <?php endif ?>

      <div class="container py-5 ">
        <div class="dark-form-content col-width-50">
          <div class="dark-form-header">
            <h2>Edytuj dane samochodu</h2>
          </div>
          <div class="dark-form-body">
            <form method="POST">
              <label>Numer rejestracyjny</label>
              <input type="username" id="num_rejestracyjny" class="form-control" placeholder="Numer rejestracyjny" name="num_rejestracyjny" value="<?= $result->num_rejestracyjny ?>">
              <label>Typ nadwozia</label>
              <input type="username" id="typ" class="form-control" placeholder="Typ nadwozia" name="typ" value="<?= $result->typ ?>">
              <label>Marka samochodu</label>
              <input type="username" id="marka" class="form-control" placeholder="Marka samochodu" name="marka" value="<?= $result->marka ?>">
              <label>Model samochodu</label>
              <input type="username" id="model" class="form-control" placeholder="Model samochodu" name="model" value="<?= $result->model ?>">
              <label>Rok produkcji</label>
              <input type="username" id="rok_produkcji" class="form-control" placeholder="Rok produkcji" name="rok_produkcji" value="<?= $result->rok_produkcji ?>">
              <label>Numer VIN</label>
              <input type="username" id="vin" class="form-control" placeholder="VIN" name="vin" value="<?= $result->vin ?>">
              <label>Silnik</label>
              <input type="username" id="silnik" class="form-control" placeholder="Podstawowe informacje o silniku" name="silnik" value="<?= $result->silnik ?>">
              <label>Wyposażenie</label>
              <textarea class="form-control" placeholder="Wyposażenie (maks. 2000 znaków)" rows="10" id="wyposazenie" name="wyposazenie"><?= $result->wyposazenie ?></textarea>
              <label>Dostępność</label>
              <input type="username" id="dostepnosc" class="form-control" placeholder="Dostępność pojazdu. 1 - dostępny; 0 - niedostępny" name="dostepnosc" value="<?= $result->dostepnosc ?>">
              <button class="btn btn-lg light-blue-button btn-block text-center" type="submit">Zapisz</button>
            </form>
          </div>
        </div>
      </div>
