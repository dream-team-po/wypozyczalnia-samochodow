<?php
//ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class Offer extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper(array('url'));
    $this->load->library('pagination');
    $this->load->library('upload');
		$this->load->model('Offer_model');
	}



	public function index(){
		header('Refresh: 0; URL=/offer/list');
	}



	public function add(){
		if($this->session->is_admin != true){
			// *brak uprawnień*
			// przekierowanie
			header('Refresh: 0; URL=/home');
			die();
		}

		// dane zwrotne
		$data = new stdClass();

		// walidacja danych z formularza
		$this->form_validation->set_rules('samochod_id', 'Samochod_id', 'required|is_unique[oferta.samochod_id]');
		$this->form_validation->set_rules('opis', 'Opis', 'required|max_length[2000]');
		$this->form_validation->set_rules('cena', 'Cena', 'required|integer');

		if($this->form_validation->run()){
			// *dane są poprawne*

			// tablica danych do dodania oferty
			$offer_data = array(
				'samochod_id'	=> 	$this->input->post('samochod_id'),
				'opis'				=> 	$this->input->post('opis'),
				'cena'				=>	$this->input->post('cena')
			);

			if($this->Offer_model->create_offer($offer_data)){
				// *udało się dodać ofertę*

                // konfiguracja wgrywania zdjęć
                $config['file_name'] = $this->Offer_model->get_offer_id_from_car_id($offer_data['samochod_id']);
                $config['upload_path'] = './images/offers';
                $config['allowed_types'] = 'png';

                // inicjalizacja wgrywania zdjęć
                $this->upload->initialize($config);

                // wgrywanie zdjęć
                $this->upload->do_upload('userfile');

				$data->success = 'Pomyślnie udało Ci się dodać ofertę.';
				$this->load->view('header');
				$this->load->view('offer_add', $data);
				$this->load->view('footer');
			}else{
				// *nie udało się dodać oferty*
				$data->error = 'Podczas dodawania oferty nastąpił błąd.';
				$this->load->view('header');
				$this->load->view('offer_add', $data);
				$this->load->view('footer');
			}

		}else{
			// *dane nie spełniają warunków*
			// wyświetlanie widoku
			$this->load->view('header');
			$this->load->view('offer_add', $data);
			$this->load->view('footer');
		}

	}



	public function edit($offer_id){
		if($this->session->is_admin != true){
			// *brak uprawnień*
			// przekierowanie
			header('Refresh: 0; URL=/home');
			die();
		}

		if($offer_id == null){
			// *parametr jest pusty*
			// przekierowanie
			header('Refresh: 0; URL=/offer/list');
			die();
		}

		// dane zwrotne
		$data = new stdClass();

		// walidacja danych z formularza
		$this->form_validation->set_rules('samochod_id', 'Samochod_id', 'required');
		$this->form_validation->set_rules('opis', 'Opis', 'required|max_length[2000]');
		$this->form_validation->set_rules('cena', 'Cena', 'required|decimal');

		if($this->form_validation->run()){
			// *dane są poprawne*

			// dane zwrotne
			$data->result = $this->Offer_model->get_offer($offer_id);

			// tablica danych do dodania oferty
			$offer_data = array(
				'id'				=>	$offer_id,
				'samochod_id'	    => 	$this->input->post('samochod_id'),
				'opis'				=> 	$this->input->post('opis'),
				'cena'				=>	$this->input->post('cena')
			);

			if($this->Offer_model->update_offer($offer_data)){
				// *udało się zapisać ofertę*
				// dane zwrotne
				$data->result = (object) $offer_data;
				$data->success = 'Pomyślnie udało Ci się zapisać ofertę.';
				$this->load->view('header');
				$this->load->view('offer_edit', $data);
				$this->load->view('footer');

				// przekierowanie
				header('Refresh: 1; URL=/offer/info/'.$offer_id);

			}else{
				// *nie udało się zapisać oferty*
				// dane zwrotne
				$data->result = (object) $offer_data;
				$data->error = 'Podczas zapisywania oferty nastąpił błąd.';
				$this->load->view('header');
				$this->load->view('offer_edit', $data);
				$this->load->view('footer');
			}

		}else{
			// *dane nie spełniają warunków*

			// dane zwrotne
			$data->result = $this->Offer_model->get_offer($offer_id);

			// wyświetlanie widoku
			$this->load->view('header');
			$this->load->view('offer_edit', $data);
			$this->load->view('footer');
		}
	}



	public function delete($offer_id = null){
		if($this->session->is_admin != true){
			// *brak uprawnień*
			// przekierowanie
			header('Refresh: 0; URL=/home');
			die();
		}

		if($offer_id == null){
			// *parametr jest pusty*
			// przekierowanie
			header('Refresh: 0; URL=/offer/list');
			die();
		}

		if($this->Offer_model->delete_offer($offer_id)){
			// *pomyślnie usunięto ofertę*
			echo "Udało się";
			header('Refresh: 1; URL=/offer/list');
		}else{
			// *nie udało się usunąć oferty*
			echo "błąd";
			header('Refresh: 1; URL=/offer/list');
		}
	}



	public function list(){
		// dane zwrotne
		$data = new stdClass();

		// konfiguracja paginacji
		$config = array();
    $config['base_url'] = '/offer/list';
    $config['total_rows'] = 10;
    $config['per_page'] = 3;
    $config['num_links'] = 200;
    $config['uri_segment'] = 3;
    $config['full_tag_open'] = '<ul class="pagination">';
    $config['full_tag_close'] = '</ul>';
    $config['attributes'] = ['class' => 'page-link'];
    $config['first_link'] = false;
    $config['last_link'] = false;
    $config['first_tag_open'] = '<li class="page-item">';
    $config['first_tag_close'] = '</li>';
    $config['prev_link'] = '&laquo';
    $config['prev_tag_open'] = '<li class="page-item">';
    $config['prev_tag_close'] = '</li>';
    $config['next_link'] = '&raquo';
    $config['next_tag_open'] = '<li class="page-item">';
    $config['next_tag_close'] = '</li>';
    $config['last_tag_open'] = '<li class="page-item">';
    $config['last_tag_close'] = '</li>';
    $config['cur_tag_open'] = '<li class="page-item active"><a href="#" class="page-link">';
    $config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';
    $config['num_tag_open'] = '<li class="page-item">';
    $config['num_tag_close'] = '</li>';

    if($this->input->post('marka') || $this->input->post('model') || $this->input->post('typ') || $this->input->post('cena-od') || $this->input->post('cena-do')){
      // *istnieją przychodzące dane z wyszukiwania*
      // walidacja danych z formularza
      $this->form_validation->set_rules('marka', 'marka', 'alpha_numeric');
      //$this->form_validation->set_rules('model', 'model', 'trim|alpha_dash|alpha_numeric_spaces');
      $this->form_validation->set_rules('typ', 'typ nadwozia', 'alpha_numeric');
      $this->form_validation->set_rules('cena-od', 'cena-od', 'alpha_numeric');
      $this->form_validation->set_rules('cena-od', 'cena-do', 'alpha_numeric');

			if($this->form_validation->run()){
				// *dane są poprawne*
				// pobieranie danych z formularza
				$marka = $this->input->post('marka');
				$model = $this->input->post('model');
				$typ = $this->input->post('typ');
				$cena_od = $this->input->post('cena-od');
				$cena_do = $this->input->post('cena-do');

				// tablica danych do wyszukiwania
				$search_data = array(
					'marka'		=>	$marka,
					'model'		=>	$model,
					'typ'		  =>	$typ,
					'cena-od'	=>	$cena_od,
					'cena-do'	=>	$cena_do
				);

				// pobieranie danych z bazy
				$data->result = $this->Offer_model->search($search_data);
				$data->marks = $this->Offer_model->get_car_marks();
				$data->types = $this->Offer_model->get_car_types();
				$data->pagination = $this->pagination->create_links();

        // dodawanie ścieżki zdjęć do wyniku
        foreach($data->result as $row){
            $offer_id = $row->ID;
            if(file_exists('./images/offers/'.$offer_id.'.png')){
                $row->image = '/images/offers/'.$offer_id.'.png';
            }else{
                $row->image = '/images/offers/brak.png';
            }
        }

				// ładowanie widoku
				$this->load->view('header');
				$this->load->view('offer_list', $data);
				$this->load->view('footer');

			}else{
				// *dane nie spełniają warunków*
				$data->marks = $this->Offer_model->get_car_marks();
				$data->types = $this->Offer_model->get_car_types();
				$data->pagination = $this->pagination->create_links();
				$this->load->view('header');
				$this->load->view('offer_list', $data);
				$this->load->view('footer');
			}

		}else{
			// *brak przychodzących danych z wyszukiwania*
      $config['total_rows'] = $this->Offer_model->count_offer_list();

      // inicjalizacja paginacji
      $this->pagination->initialize($config);

			// pobieranie danych z bazy
			$data->result = $this->Offer_model->get_offer_list($config['per_page'], $rows = $this->uri->segment(3));
			$data->marks = $this->Offer_model->get_car_marks();
			$data->types = $this->Offer_model->get_car_types();
			$data->pagination = $this->pagination->create_links();

      // dodawanie ścieżki zdjęć do wyniku
      foreach($data->result as $row){
          $offer_id = $row->ID;
          if(file_exists('./images/offers/'.$offer_id.'.png')){
              $row->image = '/images/offers/'.$offer_id.'.png';
          }else{
              $row->image = '/images/offers/brak.png';
          }
      }

			// ładowanie widoku
			$this->load->view('header');
			$this->load->view('offer_list', $data);
			$this->load->view('footer');
    }

	}



	public function search(){

		// dane zwrotne
		$data = new stdClass();

		if($this->input->post('marka')){
			// *istnieją dane POST*
			// walidacja danych z formularza
			$this->form_validation->set_rules('marka', 'marka', 'alpha_numeric');

			if($this->form_validation->run()){
				// *dane z formularza są poprawne*
				$marka = $this->input->post('marka');
				$data->models = $this->Offer_model->get_car_models_from_car_mark($marka);
				echo json_encode($data);
			}

		}

	}



	public function info($offer_id = null){

		if($offer_id == null){
			// *brak id*
			// przekierowanie
			header('Refresh: 0; URL=/offer/list');
			die();
		}

		// dane zwrotne
		$data = new stdClass();
		$data->offer_id = $offer_id;

		// pobieranie danych z bazy
		$data->result = $this->Offer_model->get_offer_info($offer_id);
		$data->opinion = $this->Offer_model->check_if_can_add_opinion($this->session->username, $offer_id);

		// dodawanie ścieżki zdjęcia
		if(file_exists('./images/offers/'.$data->result->ID.'.png')){
			$data->image = '/images/offers/'.$data->result->ID.'.png';
		}else{
			$data->image = '/images/offers/brak.png';
		}

		// ładowanie widoku
		$this->load->view('header');
		$this->load->view('offer_info', $data);
		$this->load->view('footer');

	}

}
