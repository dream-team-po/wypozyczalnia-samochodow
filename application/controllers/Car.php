<?php
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class Car extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper(array('url'));
		$this->load->model('Car_model');
	}



	public function index(){
		if(!$this->session->userdata('is_admin') == TRUE){
			// *sesja istnieje*
			header('Refresh: 0; URL=/home');
			die();
		}else{
			header('Refresh: 0; URL=/car/list');
		}
	}



	public function add(){
		if(!$this->session->userdata('is_admin') == TRUE){
			// *sesja istnieje*
			// przekierowanie
			header('Refresh: 0; URL=/home');
			die();
		}

		// dane zwrotne
		$data = new stdClass();

		// walidacja danych z formularza
		$this->form_validation->set_rules('num_rejestracyjny', 'num_rejestracyjny', 'required');
		$this->form_validation->set_rules('typ', 'typ', 'required');
		$this->form_validation->set_rules('marka', 'marka', 'required');
		$this->form_validation->set_rules('model', 'model', 'required');
		$this->form_validation->set_rules('rok_produkcji', 'rok_produkcji', 'required');
		$this->form_validation->set_rules('vin', 'vin', 'required');
		$this->form_validation->set_rules('silnik', 'silnik', 'required');
		$this->form_validation->set_rules('wyposazenie', 'wyposazenie', 'required');
		$this->form_validation->set_rules('dostepnosc', 'dostepnosc', 'required');

		if($this->form_validation->run()){
			// *dane są poprawne*
			// tablica danych do dodawania dodawania samochodu
			$car_data = array(
				'num_rejestracyjny'	=>	$this->input->post('num_rejestracyjny'),
				'typ'								=>	$this->input->post('typ'),
				'marka'							=>	$this->input->post('marka'),
				'model'							=>	$this->input->post('model'),
				'rok_produkcji'			=>	$this->input->post('rok_produkcji'),
				'vin'								=>	$this->input->post('vin'),
				'silnik'						=>	$this->input->post('silnik'),
				'wyposazenie'				=>	$this->input->post('wyposazenie'),
				'dostepnosc'				=>	$this->input->post('dostepnosc')
			);

			if($this->Car_model->create_car($car_data)){
				// *pomyślnie udało się dodać samochód*
				$data->success = 'Pomyślnie udało Ci się dodać samochód.';
				$this->load->view('header');
				$this->load->view('car_add', $data);
				$this->load->view('footer');
			}else{
				// *nie udało się dodać samochodu*
				$data->error = 'Podczas dodawania samochodu nastąpił błąd.';
				$this->load->view('header');
				$this->load->view('car_add', $data);
				$this->load->view('footer');
			}

		}else{
			// *dane nie spełniają warunków*
			$this->load->view('header');
			$this->load->view('car_add');
			$this->load->view('footer');
		}
	}



	public function edit($car_id = null){

		if(!$this->session->userdata('is_admin') == TRUE){
			// *sesja istnieje*
			// przekierowanie
			header('Refresh: 0; URL=/home');
			die();
		}

		if($car_id == null){
			// *brak id*
			// przekierowanie
			header('Refresh: 0; URL=/car/list');
			die();
		}

		// dane zwrotne
		$data = new stdClass();

		// walidacja danych z formularza
		$this->form_validation->set_rules('num_rejestracyjny', 'num_rejestracyjny', 'required');
		$this->form_validation->set_rules('typ', 'typ', 'required');
		$this->form_validation->set_rules('marka', 'marka', 'required');
		$this->form_validation->set_rules('model', 'model', 'required');
		$this->form_validation->set_rules('rok_produkcji', 'rok_produkcji', 'required');
		$this->form_validation->set_rules('vin', 'vin', 'required');
		$this->form_validation->set_rules('silnik', 'silnik', 'required');
		$this->form_validation->set_rules('wyposazenie', 'wyposazenie', 'required');
		$this->form_validation->set_rules('dostepnosc', 'dostepnosc', 'required');

		if($this->form_validation->run()){
			// *dane są poprawne*
			// tablica danych
			$car_data = array(
				'id'								=>	$car_id,
				'num_rejestracyjny'	=>	$this->input->post('num_rejestracyjny'),
				'typ'								=>	$this->input->post('typ'),
				'marka'							=>	$this->input->post('marka'),
				'model'							=>	$this->input->post('model'),
				'rok_produkcji'			=>	$this->input->post('rok_produkcji'),
				'vin'								=>	$this->input->post('vin'),
				'silnik'						=>	$this->input->post('silnik'),
				'wyposazenie'				=>	$this->input->post('wyposazenie'),
				'dostepnosc'				=>	$this->input->post('dostepnosc')
			);

			if($this->Car_model->update_car($car_data)){
				// *udało się zapisać samochód*
				// dane zwrotne
				$data->result = (object) $car_data;
				$data->success = 'Pomyślnie udało Ci się zapisać samochód.';
				$this->load->view('header');
				$this->load->view('car_edit', $data);
				$this->load->view('footer');

				// przekierowanie
				header('Refresh: 1; URL=/car/list');
			}else{
				// *nie udało się zapisać samochodu*
				// dane zwrotne
				$data->result = (object) $car_data;
				$data->error = 'Podczas zapisywania samochodu nastąpił błąd.';
				$this->load->view('header');
				$this->load->view('car_edit', $data);
				$this->load->view('footer');
			}

		}else{
			// *dane nie spełniają warunków*
			// dane zwrotne
			$data->result = $this->Car_model->get_car($car_id);

			// ładowanie widoku
			$this->load->view('header');
			$this->load->view('car_edit', $data);
			$this->load->view('footer');
		}

	}



	public function delete($car_id = null){
		if(!$this->session->userdata('is_admin') == TRUE){
			// *sesja istnieje*
			// przekierowanie
			header('Refresh: 0; URL=/home');
			die();
		}

		if($car_id == null){
			// *brak id*
			// przekierowanie
			header('Refresh: 0; URL=/car/list');
			die();
		}

		if($this->Car_model->delete_car($car_id)){
			// *pomyślnie usunięto samochód*
			echo "Udało się";
			header('Refresh: 1; URL=/car/list');
		}else{
			// *nie udało się usunąć samochodu*
			echo "błąd";
			header('Refresh: 1; URL=/car/list');
		}

	}



	public function list(){
		$data = new stdClass();
		// pobieranie danych z bazy
		$data->result = $this->Car_model->get_car_list();

		// ładowanie widoku
		$this->load->view('header');
		$this->load->view('car_list', $data);
		$this->load->view('footer');
	}



	public function rent($offer_id = null){

		if(!$this->session->has_userdata('logged_in')){
			// *sesja istnieje*
			// przekierowanie
			header('Refresh: 0; URL=/user/login');
			die();
		}

		if($offer_id == null){
			// *brak id*
			// przekierowanie
			header('Refresh: 0; URL=/offer/list');
			die();
		}

		// dane zwrotne
		$data = new stdClass();

		// walidacja danych z formularza
		$this->form_validation->set_rules('termin', 'termin', 'required');

		if($this->form_validation->run()){
			// *dane są poprawne*

			$termin = $this->input->post('termin');
			$data_wypozyczenia = substr($termin, 0, 10);
			$data_zwrotu = substr($termin, 13, 23);

			$rent_data = array(
				'data_wypozyczenia'	=>	$data_wypozyczenia,
				'data_zwrotu'				=>	$data_zwrotu,
				'samochod_id'				=>	$this->Car_model->get_car_id_from_offer_id($offer_id),
				'uzytkownik_id'			=>	$this->Car_model->get_user_id_from_username($this->session->username)
			);

			if($this->Car_model->check_date_range($offer_id ,$data_wypozyczenia, $data_zwrotu)){
			  // *Podany zakres dat nie pokrywa się z zajętymi*
        if($this->Car_model->create_rent($rent_data)){
          // *pomyślnie udało się dodać wypożyczenie*
          // dane zwrotne
          $data->result = (object) $rent_data;
          $data->price = $this->Car_model->get_price_from_offer_id($offer_id);
          $data->offer_id = $offer_id;
          $data->success = 'Pomyślnie udało Ci się wypożyczyć samochód.';
          $this->load->view('header');
          $this->load->view('car_rent', $data);
          $this->load->view('footer');

          // przekierowanie
          header('Refresh: 1; URL=/offer/list');
        }else{
          // *nie udało się dodać wypożyczenia*
          // dane zwrotne
          $data->result = (object) $rent_data;
          $data->price = $this->Car_model->get_price_from_offer_id($offer_id);
          $data->offer_id = $offer_id;
          $data->error = 'Podczas zapisywania samochodu nastąpił błąd.';
          $this->load->view('header');
          $this->load->view('car_rent', $data);
          $this->load->view('footer');
        }

      }else{
        // *Podany zakres dat pokrywa się z zajętymi*
        // dane zwrotne
        $data->result = (object) $rent_data;
        $data->price = $this->Car_model->get_price_from_offer_id($offer_id);
        $data->offer_id = $offer_id;
        $data->error = 'Podany okres nachodzi na zajęte daty.';
        $this->load->view('header');
        $this->load->view('car_rent', $data);
        $this->load->view('footer');
      }

		}else{
			// *dane nie spełniają warunków*
			$data->price = $this->Car_model->get_price_from_offer_id($offer_id);
			$data->offer_id = $offer_id;

			// ładowanie widoku
			$this->load->view('header');
			$this->load->view('car_rent', $data);
			$this->load->view('footer');
		}

	}



	public function reserved_date_ranges(){
    // dane zwrotne
    $data = new stdClass();

    if($this->input->post('offer_id')){
      // *istnieją dane POST*
      // walidacja danych z formularza
      $this->form_validation->set_rules('offer_id', 'offer_id', 'is_natural');

      if($this->form_validation->run()){
        // *dane z formularza są poprawne*
        $offer_id = $this->input->post('offer_id');
        $data->reserved_date_ranges = $this->Car_model->get_reserved_date_ranges($offer_id);
        echo json_encode($data);
      }

    }
  }

}
