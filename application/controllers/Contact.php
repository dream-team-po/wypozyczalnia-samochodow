<?php
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper(array('url'));
		$this->load->model('Contact_model');
	}



	public function index(){

	}



	public function question(){

		// dane zwrotne
		$data = new stdClass();

		// walidacja danych z formularza
		$this->form_validation->set_rules('temat', 'temat', 'required');
		$this->form_validation->set_rules('pytanie', 'pytanie', 'required');

		if($this->form_validation->run()){
			// *dane są poprawne*
			// tablica danych
			$question_data = array(
				'uzytkownik_id'	=>	$this->Contact_model->get_user_id_from_username($this->session->username),
				'temat'					=>	$this->input->post('temat'),
				'pytanie'				=>	$this->input->post('pytanie')
			);

			if($this->Contact_model->new_question($question_data)){
				// *dodano nowe pytanie do bazy*
				$data->success = 'Twoje pytanie zostało przesłane.';
				$this->load->view('header');
				$this->load->view('contact_question', $data);
				$this->load->view('footer');

			}else{
				// *nie udało się dodać nowego pytania do bazy*
				$data->error = 'Nie udało się wysłać pytania.';
				$this->load->view('header');
				$this->load->view('contact_question', $data);
				$this->load->view('footer');
			}

		}else{
			// *dane nie spełniają warunków*
			$this->load->view('header');
			$this->load->view('contact_question');
			$this->load->view('footer');
		}

	}



}
