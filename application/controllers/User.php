<?php
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper(array('url'));
		$this->load->model('User_model');
	}



	public function index(){

		if($this->session->has_userdata('logged_in')){
			// *sesja istnieje*
			// przekierowanie
			header('Refresh: 0; URL=/user/edit');
		}else{
			header('Refresh: 0; URL=/home');
			die();
		}

	}



	public function edit(){

		if(!$this->session->has_userdata('logged_in')){
			// *sesja istnieje*
			// przekierowanie
			header('Refresh: 0; URL=/user/login');
			die();
		}

		// dane zwrotne
		$data = new stdClass();

		// walidacja danych z formularza
		$this->form_validation->set_rules('email', 'Email', 'required');

		if($this->form_validation->run()){
			// *dane są poprawne*
			// tablica danych
			$edit_data = array(
	      'username'      =>  $this->session->username,
	      'email'         =>  $this->input->post('email'),
	      'imie'          =>  $this->input->post('imie'),
	      'nazwisko'      =>  $this->input->post('nazwisko'),
	      'ulica'         =>  $this->input->post('ulica'),
	      'nr_mieszkania' =>  $this->input->post('nr_mieszkania'),
	      'miejscowosc'   =>  $this->input->post('miejscowosc'),
	    );

			if($this->User_model->update_user($edit_data)){
				// *udało się*
				// pobieranie profilu użytkownika z bazy
				$username = $this->session->username;
				$data->success = "Profil został zapisany.";
				$data->result = $this->User_model->get_user_profile($username);

				// ładowanie widoku
				$this->load->view('header');
				$this->load->view('user_edit', $data);
				$this->load->view('footer');

				// przekierowanie
				header('Refresh: 1; URL=/home');

			}else{
				// *nie udało się*
				// pobieranie profilu użytkownika z bazy
				$username = $this->session->username;
				$data->error = "Nie udało się zapisać profilu.";
				$data->result = $this->User_model->get_user_profile($username);

				// ładowanie widoku
				$this->load->view('header');
				$this->load->view('user_edit', $data);
				$this->load->view('footer');
			}

		}else{
			// *dane nie spełniają warunków*
			// pobieranie profilu użytkownika z bazy
			$username = $this->session->username;
			$data->result = $this->User_model->get_user_profile($username);

			// ładowanie widoku
			$this->load->view('header');
			$this->load->view('user_edit', $data);
			$this->load->view('footer');

		}

	}



	public function profile(){

		// dane zwrotne
		$data = new stdClass();

		// pobieranie profilu użytkownika z bazy
		$username = $this->session->username;
		$data->result = $this->User_model->get_user_profile($username);

		$this->load->view('header');
		$this->load->view('user_profile', $data);
		$this->load->view('footer');
	}



	public function register(){

		if($this->session->has_userdata('logged_in')){
			// *sesja istnieje*
			// przekierowanie
			header('Refresh: 0; URL=/home');
			die();
		}

		// dane zwrotne
		$data = new stdClass();

		// walidacja danych z formularza
		$this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_numeric|min_length[4]|is_unique[uzytkownik.username]', array('is_unique' => 'Ta nazwa użytkownika jest już zajęta.'));
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[uzytkownik.email]', array('is_unique' => 'Ten adres email został już użyty.'));
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
		$this->form_validation->set_rules('rpassword', 'Confirm Password', 'trim|required|min_length[6]|matches[password]');

		if($this->form_validation->run()){
			// *dane są poprawne*

			// tablica danych do rejestracji
			$register_data = array(
	      'username'      =>  $this->input->post('username'),
	      'email'         =>  $this->input->post('email'),
	      'haslo'         =>  $this->input->post('password'),
	      'imie'          =>  $this->input->post('imie'),
	      'nazwisko'      =>  $this->input->post('nazwisko'),
	      'ulica'         =>  $this->input->post('ulica'),
	      'nr_mieszkania' =>  $this->input->post('nr_mieszkania'),
	      'miejscowosc'   =>  $this->input->post('miejscowosc'),
	    );

			if($this->User_model->create_user($register_data)){
				// *rejestracja powiodła się*
				$data->success = 'Pomyślnie udało Ci się zarejestrować.';
				$this->load->view('header');
				$this->load->view('register', $data);
				$this->load->view('footer');

				// przekierowanie
				header('Refresh: 1; URL=/user/login');

			}else{
				// *nie udało się zarejestrować*
				$data->error = 'Podczas rejestracji nastąpił błąd.';
				$this->load->view('header');
				$this->load->view('register', $data);
				$this->load->view('footer');
			}

		}else{
			// *dane nie spełniają warunków*
			$this->load->view('header');
			$this->load->view('register');
			$this->load->view('footer');
		}
	}



	public function login(){

		if($this->session->has_userdata('logged_in')){
			// *sesja istnieje*
			// przekierowanie
			header('Refresh: 0; URL=/home');
			die();
		}

		// dane zwrotne
		$data = new stdClass();

		// walidacja danych z formularza
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if($this->form_validation->run()){
			// *dane są poprawne*
			// pobieranie danych z formularza
			$username = $this->input->post('username');
			$password = $this->input->post('password');

			if($this->User_model->check_user_login($username, $password)){
				// *logowanie przeszło*
				$user_id = $this->User_model->get_user_id_from_username($username);
				$user = $this->User_model->get_user($user_id);

				// tworzenie sesji
				$session_data = array(
        	'username' 			=> 	$user->username,
					'email'					=>	$user->email,
					'imie'					=>	$user->imie,
					'nazwisko'			=>	$user->nazwisko,
					'ulica'					=>	$user->ulica,
					'nr_mieszkania'	=>	$user->nr_mieszkania,
					'miejscowosc'		=>	$user->miejscowosc,
        	'user_id'   		=> 	$user->id,
        	'logged_in' 		=> 	TRUE,
					'is_admin' 			=> 	(bool)$user->is_admin,
					'navbar'				=>	$this->User_model->get_navbar_permission($user->is_admin)
				);

				$this->session->set_userdata($session_data);

				// ładowanie widoku
				$data->success = 'Zostałeś pomyślnie zalogowany.';
				$this->load->view('header');
				$this->load->view('login', $data);
				$this->load->view('footer');

				// przekierowanie
				header('Refresh: 1; URL=/home');

			}else{
				// *logowanie odrzucono*
				$data->error = 'Błędna nazwa użytkownika lub hasło.';
				$this->load->view('header');
				$this->load->view('login', $data);
				$this->load->view('footer');
			}

		}else{
			// *dane nie spełniają warunków*
			$this->load->view('header');
			$this->load->view('login');
			$this->load->view('footer');
		}

	}



	public function logout(){

		if ($this->session->has_userdata('logged_in') && $this->session->userdata('logged_in') == true){
			// *sesja istnieje*
			// usuwanie sesji
			$this->session->sess_destroy();

			// przekierowanie na główną stronę
			header('Refresh: 0; URL=/home');

		}else{
			// *sesja nie istnieje*
			// przekierowanie na główną stronę
			header('Refresh: 0; URL=/home');
		}

	}

}
