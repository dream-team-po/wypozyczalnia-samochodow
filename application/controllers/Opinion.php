<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Opinion extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper(array('url'));
    $this->load->library('pagination');
		$this->load->model('Opinion_model');
	}



	public function index(){

	}



	public function add($offer_id = null){

		if($offer_id == null){
			// *brak id*
			// przekierowanie
			header('Refresh: 0; URL=/home');
			die();
		}

		if($this->Opinion_model->check_if_was_renting($this->session->username, $offer_id) == false){
			// *użytkownik nie wypożyczał tego samochodu*
			// przekierowanie
			header('Refresh: 0; URL=/home');
			die();
		}

		// dane zwrotne
		$data = new stdClass();

		// walidacja danych z formularza
		$this->form_validation->set_rules('ocena', 'ocena', 'required|integer');
		$this->form_validation->set_rules('opis', 'Opis', 'max_length[2000]');

		if($this->form_validation->run()){
			// *dane są poprawne*
			// tablica danych do dodania oferty
			$opinion_data = array(
				'samochod_id'	=>	$offer_id,
				'ocena'				=>	$this->input->post('ocena'),
				'opis'				=>	$this->input->post('opis')
			);

			if($this->Opinion_model->create_opinion($opinion_data, $this->session->username)){
				// *pomyślnie dodano nową opinię*
				$data->success = 'Pomyślnie udało Ci się dodać opinię.';
				$this->load->view('header');
				$this->load->view('opinion_add', $data);
				$this->load->view('footer');
			}else{
				// *nie udało się dodać nowej opinii*
				$data->error = 'Podczas dodawania opinii nastąpił błąd.';
				$this->load->view('header');
				$this->load->view('opinion_add', $data);
				$this->load->view('footer');
			}

		}else{
			// *dane nie spełniają warunków*
			// ładowanie widoku
			$this->load->view('header');
			$this->load->view('opinion_add', $data);
			$this->load->view('footer');
		}

	}


	public function list($offer_id = null){

		if($offer_id == null){
			// *brak id*
			// przekierowanie
			header('Refresh: 0; URL=/home');
			die();
		}

		// dane zwrotne
		$data = new stdClass();

    // konfiguracja paginacji
    $config = array();
    $config['base_url'] = '/opinion/list/'.$offer_id;
    $config['per_page'] = 3;
    $config['num_links'] = 200;
    $config['uri_segment'] = 4;
    $config['full_tag_open'] = '<ul class="pagination">';
    $config['full_tag_close'] = '</ul>';
    $config['attributes'] = ['class' => 'page-link'];
    $config['first_link'] = false;
    $config['last_link'] = false;
    $config['first_tag_open'] = '<li class="page-item">';
    $config['first_tag_close'] = '</li>';
    $config['prev_link'] = '&laquo';
    $config['prev_tag_open'] = '<li class="page-item">';
    $config['prev_tag_close'] = '</li>';
    $config['next_link'] = '&raquo';
    $config['next_tag_open'] = '<li class="page-item">';
    $config['next_tag_close'] = '</li>';
    $config['last_tag_open'] = '<li class="page-item">';
    $config['last_tag_close'] = '</li>';
    $config['cur_tag_open'] = '<li class="page-item active"><a href="#" class="page-link">';
    $config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';
    $config['num_tag_open'] = '<li class="page-item">';
    $config['num_tag_close'] = '</li>';
    $config['total_rows'] = $this->Opinion_model->count_opinion_list($offer_id);

    // inicjalizacja paginacji
    $this->pagination->initialize($config);

		// dane z bazy
		$data->result = $this->Opinion_model->get_opinion_list($offer_id, $config['per_page'], $rows = $this->uri->segment(4));
		$data->offer_info = $this->Opinion_model->get_offer_info($offer_id);
		$data->avg_rating = $this->Opinion_model->avg_rating($offer_id);
    $data->pagination = $this->pagination->create_links();

    // dodawanie ścieżki zdjęcia
    if(file_exists('./images/offers/'.$offer_id.'.png')){
      $data->image = '/images/offers/'.$offer_id.'.png';
    }else{
      $data->image = '/images/offers/brak.png';
    }

		// ładowanie widoku
		$this->load->view('header');
		$this->load->view('opinion_list', $data);
		$this->load->view('footer');

	}



}
