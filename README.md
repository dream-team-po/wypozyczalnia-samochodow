Przed rozpoczęciem pracy zawsze pobieraj aktualne pliki!
## Instrukcja pobierania plików
1. Wejdź do folderu i kliknij prawym
2. Wybierz Git BASH Here.
3. Następnie wpisujesz komendy:
```bash
  git pull
```
